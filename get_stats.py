import sqlite3
import logging
import requests
import pandas as pd
from datetime import datetime

logger = logging.getLogger(__name__)

con = sqlite3.connect("rwstats.sqlite")
cur = con.cursor()
regio_wikis = pd.read_sql("SELECT rowid, * FROM regio_wikis", con)

S = requests.Session()

PARAMS = {
    "action": "query",
    "meta": "siteinfo",
    "siprop": "statistics",
    "formatversion": "2",
    "format": "json"
}

rwstats = []
for id, row in regio_wikis.iterrows():
    try:
        R = S.get(url=row['api_url'], params=PARAMS)
        data = R.json()['query']['statistics']
        item = (
            row['rowid'],
            datetime.now().isoformat(),
            data['pages'],
            data['articles'],
            data['edits'],
            data['images'],
            data['users'],
            data['activeusers'])
        rwstats.append(item)
    except Exception as e:
        # Something went wrong, so nothing will be logged this time.
        logger.exception(e)

cur.executemany("INSERT INTO stats VALUES(?, ?, ?, ?, ?, ?, ?, ?)", rwstats)
con.commit()
