# Import packages
from dash import Dash, html, dcc
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import sqlite3

# Initialize the app
application = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

app = application.server
con = sqlite3.connect("rwstats.sqlite", check_same_thread=False)
cur = con.cursor()

navbar = dbc.Navbar(
    html.A(
        dbc.NavbarBrand("Regiowiki-Monitor", className="ms-2"),
        href="https://rwmon.toolforge.org/",
        style={"textDecoration": "none"}
    ),
    color="dark",
    dark=True,
)


def get_daily_new_edits_card():
    daily_new_edits = pd.read_sql("SELECT * FROM daily_new_edits", con) \
        .groupby(['title']).mean(numeric_only=True) \
        .sort_values(['newedits'], ascending=False).head(10).reset_index()
    return dbc.Card(
        dbc.CardBody([
            html.H4("Tägliche neue Bearbeitungen", className="card-title"),
            dcc.Graph(figure=px.histogram(
                daily_new_edits,
                x='newedits',
                y='title',
                histfunc='avg',
                log_x=True)
                .update_yaxes(categoryorder='total ascending')
                .update_layout(
                    xaxis_title=None,
                    yaxis_title=None,
                    margin={"l": 5, "r": 5, "t": 20, "b": 5})
            )
        ])
    )


def get_active_users_card():
    active_users = pd.read_sql("SELECT * FROM monthly_active_users", con) \
        .groupby(['title']).mean(numeric_only=True) \
        .sort_values(['active_users'], ascending=False).head(10).reset_index()
    return dbc.Card(
        dbc.CardBody([
            html.H4("Aktive Autorinnen und Autoren", className="card-title"),
            dcc.Graph(figure=px.histogram(
                active_users,
                x='active_users',
                y='title',
                histfunc='avg')
                .update_yaxes(categoryorder='total ascending')
                .update_layout(
                    xaxis_title=None,
                    yaxis_title=None,
                    margin={"l": 5, "r": 5, "t": 20, "b": 5})
            )
        ])
    )


def get_wiki_table():
    wiki_info = pd.read_sql("SELECT * FROM wiki_info", con)
    last_7_days = pd.read_sql("SELECT * FROM last_weeks_activity", con)
    table_header = [html.Thead(html.Tr([
        html.Th("Wiki"),
        html.Th("Bearbeitungen der letzten 7 Tage"),
        html.Th("Letzte Aktivität")]))]
    table_body = [html.Tbody([html.Tr([
        html.Td(row["title"]),
        html.Td(dcc.Graph(figure=px.bar(
                last_7_days[last_7_days.wiki_id == id+1],
                x='day',
                y='newedits',
                width=350,
                height=150)
                .update_xaxes(dtick="d1", tickformat="%a")
                .update_yaxes(showticklabels=False)
                .update_layout(
                    xaxis_title=None,
                    yaxis_title=None,
                    margin={"l": 0, "r": 0, "t": 0, "b": 0}))),
        html.Td(row["latest_edit"])]) for id, row in wiki_info.iterrows()])]
    return dbc.Table(table_header + table_body, bordered=True)


def serve_layout():
    return dbc.Container([
        navbar,
        dbc.Container([
            html.H4('TOP 10 des letzten Monats'),
            dbc.Row([
                dbc.Col(get_daily_new_edits_card()),
                dbc.Col(get_active_users_card())
            ],
                className="mb-3"
            ),
            html.H4('Liste aller Regionalwikis'),
            get_wiki_table()
        ],
            className="p-3 bg-light"
        )
    ])


application.layout = serve_layout

# Run the app
if __name__ == '__main__':
    application.run(debug=True)
